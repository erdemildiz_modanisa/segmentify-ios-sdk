//
//  ProductModel.swift
//  Segmentify
//
//  Created by Ata Anıl Turgay on 23.01.2018.
//  Copyright © 2018 segmentify. All rights reserved.
//

import Foundation

public enum NotificationType:String,Codable {
   case VIEW,CLICK,PERMISSION_INFO,SUBSCRIBE_TOPIC,UNSUBSCRIBE_TOPIC
}
