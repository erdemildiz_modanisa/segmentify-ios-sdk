//
//  AssetType.swift
//  Pods
//
//  Created by Ebru Goksal on 8.01.2020.
//

import Foundation

public enum AssetType:String {
    case CATEGORY, BRAND, KEYWORD
    
}
